package model;

public enum CarType {
  BMW("BMW"), MERCEDES("Mercedes"), AUDY("Audy"), DEFAULT("");

  private final String name;

  CarType(String name) {
    this.name = name;
  }

  public static CarType getByName(String name) {
    switch (name) {
      case "BMW" : return BMW;
      case "Mercedes" : return MERCEDES;
      case "Audy" : return AUDY;
    }
    return null;
  }

  @Override
  public String toString() {
    return name;
  }
}
