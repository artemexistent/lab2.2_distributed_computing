package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car {
  private CarType type;
  private Engine engine;
  private String color;
  private String model;
  private String serialNumber;
  private String number;

  public int getEngineFuelUse() {
    return engine.getFuelUse();
  }
}