package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Engine {
  private int id;
  private String name;
  private int fuelUse;
  private int maxSpeed;
  private int horsepower;
}
