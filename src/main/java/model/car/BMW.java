package model.car;

import model.Car;
import model.Engine;

import static model.CarType.BMW;

public class BMW extends Car {
  public BMW() {
    super(BMW, null, null, null, null, null);
  }

  public BMW(Engine engine) {
    super(BMW, engine, null, null, null, null);
  }
}
