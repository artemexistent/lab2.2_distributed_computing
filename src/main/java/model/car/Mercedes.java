package model.car;

import model.Car;
import model.Engine;

import static model.CarType.MERCEDES;

public class Mercedes extends Car {
  public Mercedes() {
    super(MERCEDES, null, null, null, null, null);
  }

  Mercedes(Engine engine) {
    super(MERCEDES, engine, null, null, null, null);
  }
}
