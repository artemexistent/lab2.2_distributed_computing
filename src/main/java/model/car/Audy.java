package model.car;

import model.Car;
import model.Engine;

import static model.CarType.AUDY;

public class Audy extends Car {
  public Audy(){
    super(AUDY, null, null, null, null, null);
  }
  Audy(Engine engine) {
    super(AUDY, engine, null, null, null, null);
  }
}
