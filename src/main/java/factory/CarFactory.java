package factory;

import model.Car;
import model.CarType;
import model.car.Audy;
import model.car.BMW;
import model.car.Mercedes;

public class CarFactory {

  public static Car getCar(CarType type) {
    switch (type) {
      case BMW: return new BMW();
      case AUDY: return new Audy();
      case MERCEDES: return new Mercedes();
      default: return new Car();
    }
  }
}
