package domain;


import lombok.SneakyThrows;

import java.io.IOException;
import java.util.Properties;

public class ApplicationProperties {

  private static final String configFile = "application.properties";
  private static Properties properties = null;


  @SneakyThrows
  private static Properties getProperties() {
    if (properties == null) {
      properties = new Properties();
      properties.load(ApplicationProperties.class.getClassLoader().getResourceAsStream(configFile));
    }
    return properties;
  }

  public static String getAppProperties(String name) {
    System.out.println(getProperties().getProperty(name));
    return getProperties().getProperty(name);
  }

}
