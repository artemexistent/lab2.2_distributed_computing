import factory.CarFactory;
import lombok.SneakyThrows;
import model.Car;
import model.CarType;
import model.Engine;
import service.TaxiService;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import static model.CarType.AUDY;
import static model.CarType.BMW;
import static model.CarType.DEFAULT;
import static model.CarType.MERCEDES;

public class Main {

  private static final TaxiService taxiService = new TaxiService();
  private static Scanner in = new Scanner(System.in);

  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    String command = in.nextLine();
    while (!command.equals("exit")) {
      switch (command) {
        case "add car":
          addCar(DEFAULT);
          break;
        case "add bmw":
          addCar(BMW);
          break;
        case "add mercedes":
          addCar(MERCEDES);
          break;
        case "add audy":
          addCar(AUDY);
          break;
        case "get car all":
          getCarAll();
          break;
        case "get cost":
          System.out.println(taxiService.getCost());
          break;
        case "add engine":
          addEngine();
          break;
        case "get engine":
          getEngine();
          break;
        case "get car by range":
          getCarByRange();
          break;
        default:
          System.out.println("Application doesn't know this command");
          break;
      }
      command = in.nextLine();
    }
  }

  @SneakyThrows
  private static void getCarByRange() {
    System.out.println("input left and right ranges:");
    String[] ranges = in.nextLine().split(" ");
    int left = Integer.parseInt(ranges[0]);
    int right = Integer.parseInt(ranges[1]);
    List<Car> allCar = taxiService.findCarByRange(left, right);
    for (Car car : allCar) {
      System.out.println(car.toString());
    }
  }

  @SneakyThrows
  private static void addCar(CarType type) {
    System.out.println("input what you want to add:");
    Car car = CarFactory.getCar(type);
    int timeParking = 0;
    String input = in.nextLine();
    while (!input.equals("add")) {
      switch (input) {
        case "id_engine":
          int engineId = in.nextInt();
          in.nextLine();
          Engine engine = taxiService.getEngineById(engineId);
          car.setEngine(engine);
          break;
        case "number":
          car.setNumber(in.nextLine());
          break;
        case "car_type":
          if (car.getType() != null) {
            System.out.println("car has already had type");
            break;
          }
          String carType = in.nextLine();
          type = CarType.getByName(carType);
          car.setType(type);
          break;
        case "color":
          car.setColor(in.nextLine());
          break;
        case "model":
          car.setModel(in.nextLine());
          break;
        case "serial_number":
          car.setSerialNumber(in.nextLine());
          break;
        case "time_parking":
          timeParking = in.nextInt();
          in.nextLine();
          break;
        case "exit":
          return;
        default:
          System.out.println("Application doesn't know this command");
          break;
      }

      System.out.println("input what you want to add:");
      input = in.nextLine();
    }
    if (taxiService.add(car, timeParking)) {
      System.out.println("Addition has been success");
    } else {
      System.out.println("Addition has been failed");
    }
  }

  @SneakyThrows
  private static void addEngine() {
    System.out.println("input what you want to add:");
    String input = in.nextLine();
    Engine engine = new Engine();
    while (!input.equals("add")) {
      switch (input) {
        case "id":
          engine.setId(in.nextInt());
          in.nextLine();
          break;
        case "name":
          engine.setName(in.nextLine());
          break;
        case "fuel_use":
          engine.setFuelUse(in.nextInt());
          in.nextLine();
          break;
        case "max_speed":
          engine.setMaxSpeed(in.nextInt());
          in.nextLine();
          break;
        case "horsepower":
          engine.setHorsepower(in.nextInt());
          in.nextLine();
          break;
        case "exit":
          return;
        default:
          System.out.println("Application doesn't know this command");
          break;
      }
      System.out.println("input what you want to add:");
      input = in.nextLine();
    }
    if (taxiService.add(engine)) {
      System.out.println("Addition has been success");
    } else {
      System.out.println("Addition has been failed");
    }
  }

  @SneakyThrows
  private static void getEngine() {
    System.out.println("input engine id:");
    int id = in.nextInt();
    in.nextLine();
    Engine engine = taxiService.getEngineById(id);
    System.out.println(engine);
  }

  private static void getCarAll() throws SQLException, ClassNotFoundException {
    List<Car> allCar = taxiService.getAllCar();
    for (Car car : allCar) {
      System.out.println(car.toString());
    }
  }
}
