package service;

import model.Car;
import model.Engine;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaxiService {

  private static final DataBaseService dataBase = DataBaseService.init();

  public int getCost() throws SQLException, ClassNotFoundException {
    Map<Car, Integer> allCar = dataBase.getAllCar();
    return allCar.values().stream()
        .reduce(Integer::sum).orElse(0) * 2;
  }

  public List<Car> getAllCar() throws SQLException, ClassNotFoundException {
    Map<Car, Integer> allCar = dataBase.getAllCar();
    return allCar.keySet().stream()
        .sorted(Comparator.comparing(Car::getEngineFuelUse))
        .collect(Collectors.toList());
  }

  public boolean add(Car car, int timeParking) {
    try {
      dataBase.insert(car, timeParking);
      return true;
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
      return false;
    }
  }

  public boolean add(Engine engine) {
    try {
      dataBase.insert(engine);
      return true;
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
      return false;
    }
  }

  public Engine getEngineById(int id) throws SQLException, ClassNotFoundException {
    Engine engine;
    engine = dataBase.findEngine(id);
    return engine;
  }

  public List<Car> findCarByRange(int left, int right) throws SQLException, ClassNotFoundException {
    Map<Car, Integer> allCar = dataBase.getAllCar();
    return allCar.keySet().stream()
        .filter(car -> car.getEngine().getMaxSpeed() >= left && car.getEngine().getMaxSpeed() <= right)
        .collect(Collectors.toList());
  }
}
