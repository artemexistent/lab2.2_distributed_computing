package service;

import domain.ApplicationProperties;
import model.Car;
import model.CarType;
import model.Engine;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

import static service.Query.*;

public class DataBaseService {
  private static String JDBC_DRIVER = "org.postgresql.Driver";
  private static String DB_URL = "jdbc:postgresql://localhost:5432/Wallet";
  private static String USERNAME = "user";
  private static String PASSWORD = "password";
  private static DataBaseService repository;

  private static Connection getConnection() throws SQLException, ClassNotFoundException {
    Connection connection;
    Class.forName(JDBC_DRIVER);
    connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
    return connection;
  }

  public static DataBaseService init() {
    if (repository != null) {
      return repository;
    }
    repository = new DataBaseService();
    JDBC_DRIVER = ApplicationProperties.getAppProperties("JDBC_DRIVER");
    DB_URL = ApplicationProperties.getAppProperties("DB_URL");
    USERNAME = ApplicationProperties.getAppProperties("USERNAME");
    PASSWORD = ApplicationProperties.getAppProperties("PASSWORD");
    return repository;
  }

  public Map<Car, Integer> getAllCar() throws SQLException, ClassNotFoundException {
    Map<Car, Integer> cars = new HashMap<>();
    try (Connection connection = getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_CAR.toString())) {
      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        String color = resultSet.getString("color");
        String model = resultSet.getString("model");
        String serial_number = resultSet.getString("serial_number");
        String number = resultSet.getString("number");
        int engineId = resultSet.getInt("id_engine");
        Engine engine = findEngine(engineId);
        CarType type = CarType.getByName(resultSet.getString("car_type"));
        Car car = new Car(type, engine, color, model, serial_number, number);
        Integer time_parking = resultSet.getInt("time_parking");
        cars.put(car, time_parking);
      }
    }
    return cars;
  }

  public void insert(Car car, int time_parking) throws SQLException, ClassNotFoundException {
    try (Connection connection = getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_CAR.toString())) {
      preparedStatement.setString(1, car.getNumber());
      preparedStatement.setInt(2, car.getEngine().getId());
      preparedStatement.setString(3, car.getType().toString());
      preparedStatement.setString(4, car.getColor());
      preparedStatement.setString(5, car.getModel());
      preparedStatement.setString(6, car.getSerialNumber());
      preparedStatement.setInt(7, time_parking);
      preparedStatement.executeUpdate();
    }
  }

  public void insert(Engine engine) throws SQLException, ClassNotFoundException {
    try (Connection connection = getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_ENGINE.toString())) {
      preparedStatement.setInt(1, engine.getId());
      preparedStatement.setString(2, engine.getName());
      preparedStatement.setInt(3, engine.getFuelUse());
      preparedStatement.setInt(4, engine.getMaxSpeed());
      preparedStatement.setInt(5, engine.getHorsepower());
      preparedStatement.executeUpdate();
    }
  }

  public Engine findEngine(int id) throws SQLException, ClassNotFoundException {
    try (Connection connection = getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(FIND_ENGINE_BY_ID.toString())){
      preparedStatement.setInt(1, id);
      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        String name = resultSet.getString("name");
        int fuelUse = resultSet.getInt("fuel_use");
        int maxSpeed = resultSet.getInt("max_speed");
        int horsepower = resultSet.getInt("horsepower");
        return new Engine(id, name, fuelUse, maxSpeed, horsepower);
      }
    }
    return null;
  }

  public Car getCarByNumber(String number) throws SQLException, ClassNotFoundException {
    try (Connection connection = getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(FIND_CAR_BY_NUMBER.toString())){
      preparedStatement.setString(1, number);
      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        String color = resultSet.getString("color");
        String model = resultSet.getString("model");
        String serial_number = resultSet.getString("serial_number");
        int engineId = resultSet.getInt("id_engine");
        Engine engine = findEngine(engineId);
        CarType type = CarType.getByName(resultSet.getString("car_type"));
        return new Car(type, engine, color, model, serial_number, number);
      }
    }
    return null;
  }

}
