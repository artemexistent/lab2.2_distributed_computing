package service;

public enum Query {
  GET_ALL_CAR("SELECT * FROM taxi_service"),
  INSERT_INTO_CAR("INSERT INTO taxi_service VALUES(?,?,?,?,?,?,?)"),
  INSERT_INTO_ENGINE("INSERT INTO engine VALUES(?,?,?,?,?)"),
  FIND_ENGINE_BY_ID("SELECT * FROM engine WHERE engine.id = ?"),
  FIND_CAR_BY_NUMBER("SELECT * FROM taxi_service WHERE taxi_service.number = ?");

  private final String query;

  Query(String query) {
    this.query = query;
  }

  @Override
  public String toString() {
    return query;
  }
}
