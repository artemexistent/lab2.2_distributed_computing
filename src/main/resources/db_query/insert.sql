insert into engine values
(1, 'first', 13, 200, 14),
(2, 'second', 15, 220, 45),
(3, 'third', 12, 100, 24),
(4, 'MD5', 10, 40, 5),
(5, 'spring', 5, 140, 14),
(6, 'KNU', 7, 130, 23),
(7, 'engine', 9, 180, 35);

insert into taxi_service values
('BT0553AC', 1, 'BMW', 'black', 'x5', '3344mmsd344d', 14),
('BT7463AC', 4, 'Mercedes', 'white', 'rtp5', 'jskdhfsd', 24),
('BT3456AC', 3, 'Audy', 'grey', '6X1seg', 'aos5A0H', 93),
('BT8387AC', 6, 'Mercedes', 'red', 'stream', '2wEn9P0', 26),
('BT5837AC', 7, 'BMW', 'silver', 'wound', '3bCC0L', 53),
('BT5747AC', 2, 'Audy', 'white', 'hotel', 'j9k6l36', 92),
('BT9274AC', 5, 'Mercedes', 'grey', 'D3q1016', 'Jkz0rl', 45)
