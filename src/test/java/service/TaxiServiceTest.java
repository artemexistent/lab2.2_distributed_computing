package service;

import lombok.SneakyThrows;
import model.Car;
import model.CarType;
import model.Engine;
import model.car.BMW;
import model.car.Mercedes;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TaxiServiceTest {

  private static final TaxiService taxiService = new TaxiService();
  private static final DataBaseService dataBaseService = DataBaseService.init();

  @SneakyThrows
  @Test
  void addCar() {
    Engine engine = Engine.builder().id(3).name("third").fuelUse(12).maxSpeed(100).horsepower(24).build();
    Car car = BMW.builder().engine(engine).number("lp5UWLF").type(CarType.BMW).build();
    assertTrue(taxiService.add(car, 93));

    Car carFromDB = dataBaseService.getCarByNumber("lp5UWLF");
    assertEquals(car, carFromDB);

    assertFalse(taxiService.add(car, 93));
  }

  @SneakyThrows
  @Test
  void addEngine() {
    Engine engine = Engine.builder().id(3).name("third").fuelUse(12).maxSpeed(100).horsepower(24).build();
    assertFalse(taxiService.add(engine));

    engine.setId(1000);
    assertTrue(taxiService.add(engine));
  }

  @SneakyThrows
  @Test
  void getEngineById() {
    Engine engine = Engine.builder().id(3).name("third").fuelUse(12).maxSpeed(100).horsepower(24).build();
    Engine engineFromDB = dataBaseService.findEngine(3);
    assertEquals(engine, engineFromDB);
  }

  @SneakyThrows
  @Test
  void findCarByRange() {
    Engine engine = Engine.builder().id(1001).name("third").fuelUse(12).maxSpeed(1000).horsepower(24).build();
    Engine engineTwo = Engine.builder().id(1002).name("third").fuelUse(12).maxSpeed(3000).horsepower(24).build();

    assertTrue(taxiService.add(engine));
    assertTrue(taxiService.add(engineTwo));

    Car bmw = BMW.builder().engine(engine).number("VR1hf5").type(CarType.BMW).build();
    Car mercedes = Mercedes.builder().engine(engineTwo).number("z3yzDYrO").type(CarType.MERCEDES).build();

    assertTrue(taxiService.add(bmw, 100));
    assertTrue(taxiService.add(mercedes, 100));

    List<Car> carByRange = taxiService.findCarByRange(1000, 3000);
    assertArrayEquals(carByRange.stream()
        .sorted(Comparator.comparing(Car::getType))
        .toArray(), new Car[]{bmw, mercedes});
  }
}