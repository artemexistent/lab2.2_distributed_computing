CREATE TABLE IF NOT EXISTS engine(
    id integer,
    name varchar(64) not null,
    fuel_use integer not null,
    max_speed integer not null,
    horsepower integer not null,
    primary key(id)
);
CREATE TABLE IF NOT EXISTS taxi_service(
    number varchar(64) primary key ,
    id_engine integer not null,
    car_type varchar(64) not null,
    color varchar(64) ,
    model varchar(64) ,
    serial_number varchar(64) ,
    time_parking integer not null,
    FOREIGN KEY (id_engine) REFERENCES engine(id)
);

